package com.example.staccoppgave.activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.staccoppgave.R
import com.example.staccoppgave.fragments.NyttLaanFragment
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.plan_activity.*
import java.time.Duration
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.concurrent.TimeUnit
import kotlin.math.abs

class PlanActivityUAPI : AppCompatActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.plan_activity)
        //Definerer grafen
        val linje = LineGraphSeries<DataPoint>()
        //Graph info
        calculateDatapoints()


        //Grafsettings
        graf_nedbetaling.viewport.setMaxX(350.0)
        graf_nedbetaling.addSeries(linje)
        graf_nedbetaling.viewport.isScalable = true
        graf_nedbetaling.viewport.isScrollable = true
        graf_nedbetaling.viewport.isXAxisBoundsManual = true
    }
    private fun calculateDatapoints() {
        //Data from user
        val lånebelop = NyttLaanFragment.laan.laanebelop
        val renter = NyttLaanFragment.laan.nominiellRente
        val gebyr = NyttLaanFragment.laan.terminGebyr
        val dagensDato = NyttLaanFragment.laan.saldoDato
        val forsteInnDato = dagensDato
        val termineringsDato = NyttLaanFragment.laan.utlopsDato



        val tid = abs(termineringsDato - dagensDato) //Tid i millisec mellom datoene
        val diffDays = tid /(24 * 60 * 60 * 1000)  // Tiden i dager

        val months = diffDays / 29
        val years = months / 12
        println(diffDays)
        Log.d("Tall" ," Dates :   " + termineringsDato + " " + dagensDato + " " + months)

       // antall_innbetalinger_out.text = months.toString()





    }
}