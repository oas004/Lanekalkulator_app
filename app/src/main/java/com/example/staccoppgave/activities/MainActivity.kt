package com.example.staccoppgave.activities

/*
Written by Odin Asbjørnsen

Oktober 2019
*/
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.staccoppgave.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //fetchJSONData()

        val fragmentAdapter =
            com.example.staccoppgave.PagerAdapter(supportFragmentManager) // For å implementere tabs til ulike fragments
        viewPager_main.adapter = fragmentAdapter
        tabs_main.setupWithViewPager(viewPager_main)


    }


}
