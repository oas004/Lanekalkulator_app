package com.example.staccoppgave.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.staccoppgave.R
import com.example.staccoppgave.fragments.NyttLaanFragment
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.plan_activity.*


class PlanActivity:AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.plan_activity)
        //Definerer grafen
        val linje = LineGraphSeries<DataPoint>()

        //Definerer teksten i fragment view
        val nedbetaling = NyttLaanFragment.nedbetalingObj
        antall_innbetalinger_out.text = nedbetaling.innbetalinger.size.toString()
        out_renter_total.text = regnUtEffektivRente().toString()
        mnd_out_innbetaling.text = innbetalingSum().toString() + " kr"

        //Grafsettings
        graf_nedbetaling.addSeries(createDataPoints(linje))
        graf_nedbetaling.viewport.isScalable = true
        graf_nedbetaling.viewport.isScrollable = true
        graf_nedbetaling.viewport.isXAxisBoundsManual = true
        graf_nedbetaling.visibility = View.VISIBLE

    }
//Lager datapunkter til grafen
    private fun createDataPoints(linje:LineGraphSeries<DataPoint>):LineGraphSeries<DataPoint> {
        val nedbetalinger = NyttLaanFragment.nedbetalingObj
        val totalList = nedbetalinger.total
        var x:Double = 0.0
        var y:Double = 0.0
        for(i in 0 until totalList.size) {

            y = totalList.get(i).toDouble()
            x = i.toDouble()
            linje.appendData(DataPoint(x, y), true, totalList.size)
        }



        return linje

    }

    //Returnerer gjennomsnitt av hver måned
    private fun innbetalingSum() :Int {

        val mengde = NyttLaanFragment.nedbetalingObj.innbetalinger

        var sumInnbetalinger = 0.0
        for(i in 0 until mengde.size) {
            sumInnbetalinger += mengde.get(i).toDouble()
        }
        val nedbetalinger = NyttLaanFragment.nedbetalingObj.innbetalinger.size
        val gjMengde = sumInnbetalinger / nedbetalinger
        return gjMengde.toInt()
    }
    // Returnerer en effektiv rente (gebyr + mnd rente)
    private fun regnUtEffektivRente() :Float {
        val nedbetalinger = NyttLaanFragment.nedbetalingObj
        val renteListe = nedbetalinger.renter
        val gebyrListe = nedbetalinger.gebyr
        var rente = 0f
        for(i in 0 until renteListe.size) {
            rente += renteListe.get(i).toFloat() +  gebyrListe.get(2).toFloat()

        }
        return rente
    }


}