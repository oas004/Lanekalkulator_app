package com.example.staccoppgave.fragments

import kotlin.*
import android.content.Intent
import android.os.Bundle
import android.util.JsonReader
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.beust.klaxon.Klaxon

import kotlin.coroutines.*
import com.example.staccoppgave.R
import com.example.staccoppgave.activities.PlanActivity
import com.example.staccoppgave.activities.PlanActivityUAPI
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.core.Json
import com.github.kittinunf.fuel.android.extension.jsonDeserializer
import com.github.kittinunf.fuel.core.*
import com.github.kittinunf.fuel.core.extensions.cUrlString
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.google.gson.*
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.nyttlaan_fragment.view.*
import kotlinx.coroutines.*
import kotlinx.serialization.SerialInfo
import kotlinx.serialization.Serializable
import okhttp3.internal.wait
import java.io.Reader
import java.io.StringReader
import kotlin.coroutines.*
import java.lang.Exception
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.typeOf


class NyttLaanFragment: Fragment() {

    companion object {
        val debugKey = "INP"
        lateinit var laan: Laan
        lateinit var nedbetalingObj: Nedbetalinger
    }

    var endretTermineringsdato = "2045-01-01"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.nyttlaan_fragment, container, false)

        //FuelManager trenger en basepath
        //FuelManager.instance.basePath = "https://visningsrom.stacc.com"

        root.utlopsdato_input.setOnDateChangeListener { calendarView, i, i2, i3 ->
            //For å få riktig format
            if (i2 <= 9) {
                endretTermineringsdato =
                    i.toString() + "-" + "0" + i2.toString() + "-" + i3.toString()
            }
            if (i3 <= 9) {
                endretTermineringsdato =
                    i.toString() + "-" + i2.toString() + "-" + "0" + i3.toString()
            }
            if (i2 <= 9 && i3 <= 9) {
                endretTermineringsdato =
                    i.toString() + "-" + "0" + i2.toString() + "-" + "0" + i3.toString()
            } else {
                endretTermineringsdato = i.toString() + "-" + i2.toString() + "-" + i3.toString()
                //root.utlopsdato_editText.text = endretTermineringsdato.toString()
            }

            Log.d(debugKey, endretTermineringsdato)

        }

        //Hva skjer når man sier hvor stort lånet er
        root.input_laan_mengde.setOnClickListener {
            root.input_laan_mengde.text.clear() //tar vekk eksempelteksten
        }
        //Hva skjer når man skal skrive inn den nominelle renten
        root.nomiell_rente_input.setOnClickListener {
            root.nomiell_rente_input.text.clear() // Tar vekk eksempeltekst
        }
        //Termingebyr input
        root.termin_gebyr_input.setOnClickListener {
            root.termin_gebyr_input.text.clear() // Tar vekk eksempeltekst
        }

        //Håndterer hva som skjer når kunden skal beregne en nedbetalingsplan
        root.kalkuler_nedbetaling_button.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main, CoroutineStart.DEFAULT) {
                makeData(root)
            }

        }




        return root
    }


    //MakeData func setter sammen data til slik api vil ha det
    //Denne funksjonen må extende view på grunn av at fragment må returnere en inflated view
    private fun makeData(root: View): View {
        //InputData fra forbruker
        try {
            // val sdf = SimpleDateFormat("yyyyMMdd")
            val sdf2 = SimpleDateFormat("dd-MM-yyyy")
            val currentData = System.currentTimeMillis()
            val datoForste = sdf2.format(Date())

            val mengde = root.input_laan_mengde.text.toString().toInt()
            val rente = root.nomiell_rente_input.text.toString().toInt()
            val gebyr = root.termin_gebyr_input.text.toString().toInt()
            val termineringsdato = root.utlopsdato_input.date
            val termineringsdato2 = root.utlopsdato_editText.text.toString() //
            Log.d("Tall", "$termineringsdato")

            laan = Laan(
                mengde,
                rente,
                gebyr,
                termineringsdato,
                currentData,
                currentData,
                "TERMINERINGSBELOP"
            )

            createJSONObject(mengde, rente, gebyr, termineringsdato2, datoForste)
        } catch (e: Throwable) {
            Log.d(debugKey, e.message)
            Toast.makeText(this@NyttLaanFragment.context, e.message, Toast.LENGTH_SHORT)
                .show() //For at brukeren skal forstå
            return root
        }



        return root
    }

    private fun createJSONObject(
        mengde: Int,
        rente: Int,
        gebyr: Int,
        termineringsdato: String,
        datoForsteInnbetaling: String
    ) {

        //Antar bruker vil ha planen fra nåværende dato
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentData = sdf.format(Date())
        Log.d(debugKey, "CreateJSONOBJECT was called " + currentData + " " + termineringsdato)


        val bodyJson = """
        { "laanebelop":$mengde,
        "nominellRente":$rente,
        "terminGebyr":$gebyr,
        "utlopsDato":"$termineringsdato",
        "saldoDato":"$currentData",
        "datoForsteInnbetaling":"$currentData",
        "ukjentVerdi" : "TERMINBELOP"
        }
        """
        sendJsonPayload(bodyJson)

    }

    private fun sendJsonPayload(obj: String) {
        //Må bruke en coroutine for at nettverket ikke skal være på mainthread.
       runBlocking {
            Log.d(debugKey, "Trying to send JsonPayload")
            this.async {
                "https://visningsrom.stacc.com/dd_server_laaneberegning/rest/laaneberegning/v1/nedbetalingsplan".httpPost()
                    .header("Content-Type" to "Application/json").body(obj)
                    .response { request: Request, response: Response, result: Result<ByteArray, FuelError> ->
                        result.success {
                            Log.d(debugKey, "SUCCESS!") // Yey! fikk hentet info fra APIet!
                            val jsoninf = Json(String(it))

                            val jsonObject = jsoninf.obj()
                            val innbetalingerJsonArray =
                                jsonObject.getJSONObject("nedbetalingsplan")
                                    .getJSONArray("innbetalinger")
                            //val nedbetalinger = Nedbetalinger()
                            // val deserializer = Deserializer().deserialize(jsonObject)
                            /*
                            val list = ArrayList<Nedbetalinger>()
                            val test = ArrayList<String>()
                            for (i in 0..innbetalingerJsonArray.length()) {

                                val innbetalingerJsonInnbetalinger =
                                    jsonObject.getJSONObject("nedbetalingsplan")
                                        .getJSONArray("innbetalinger").getJSONObject(i)
                                        .get("innbetaling").toString()
                                test.add(innbetalingerJsonInnbetalinger)
                                println(innbetalingerJsonInnbetalinger)
                                for(j in 0..innbetalingerJsonArray.length()) {

                                }

                            }*/
                            val innbetalingList = mutableListOf<String>()
                            val gebyrList = mutableListOf<String>()
                            val datoList = mutableListOf<String>()
                            val renterList = mutableListOf<String>()
                            val restgjeldList = mutableListOf<String>()
                            val totalList = mutableListOf<String>()

                            for (i in 0..innbetalingerJsonArray.length() -1) {
                                val jsObj = innbetalingerJsonArray.getJSONObject(i)

                                    val innbet = jsObj.get("innbetaling").toString()
                                    val gebyr = jsObj.get("gebyr").toString()
                                    val renter = jsObj.get("renter").toString()
                                    val dato = jsObj.get("dato").toString()
                                    val restgjeld = jsObj.get("restgjeld").toString()
                                    val total = jsObj.get("total").toString()
                                    innbetalingList.add(innbet)
                                    gebyrList.add(gebyr)
                                    datoList.add(dato)
                                    renterList.add(renter)
                                    restgjeldList.add(restgjeld)
                                    totalList.add(total)
                                    println(total)


                            }
                            //println(innbetalingList.toString())
                            val nedbetaling = Nedbetalinger(innbetalingList, datoList,gebyrList, renterList,totalList, restgjeldList)
                            nedbetalingObj = nedbetaling
                            // val nedbetalingListe: List<Nedbetalinger> = gson.fromJson(stringreader)
                            //println(innbetalingerJsonArray)


                        }
                        activity?.let {
                            val intent = Intent(it, PlanActivity::class.java)
                            it.startActivity(intent)

                        }


                        result.failure {
                            Log.d(debugKey, "FAIL.." + it.localizedMessage)
                            println(it.errorData)
                            activity?.let {
                                val intent = Intent(it, PlanActivity::class.java)
                                it.startActivity(intent)
                            }


                        }

                    }
            }
        }

    }
    /*private fun parseArray(innbetalingerJsonArray:JsonArray?) {
        val klaxon = Klaxon()
        JsonReader().use { reader ->
            val result = arrayListOf<Nedbetalinger>()
            reader.beginArray{
                while (reader.hasNext()) {
                    val nedbetaling =klaxon.parse<Nedbetalinger>(reader)
                    result.add(nedbetaling)
                }
            }
        }
    }
}*/


    //Lån data klassen for å holde
    data class Laan(
        val laanebelop: Int,
        val nominiellRente: Int,
        val terminGebyr: Int,
        val utlopsDato: Long,
        val saldoDato: Long,
        val datoForsteInnbetaling: Long,
        val ukjentVerdi: String
    )

    data class Nedbetalinger(
        val innbetalinger: List<String>,
        val dato: List<String>,
        val gebyr: List<String>,
        val renter: List<String>,
        val total: List<String>,
        val restgjeld: List<String>
    )
}

//Slik ville jeg prøvd å deserilazere dataene fra json format, men rakk ikke å bli ferdig med dette
  /*  @Serializable
    class Nedbetalinger {


        var innbetalinger: MutableCollection<Float> = mutableListOf()
        var dato: MutableCollection<String> = mutableListOf()
        var gebyrer: MutableCollection<Float> = mutableListOf()
        var renter: MutableCollection<Float> = mutableListOf()
        var total: MutableCollection<Float> = mutableListOf()
        var restgjeld: MutableCollection<Float> = mutableListOf()
    }*/
/*



        // Deserializer
        class Deserializer : JsonDeserializer<Nedbetalinger> {
            override fun deserialize(json: JsonObject):Nedbetalinger {

                val nedbetalinger = Nedbetalinger()
                val mainJsonObj = json.get("nedbetalingsplan").asJsonObject.get("innbetalinger").asJsonArray

                for(i in 0..mainJsonObj.size()) {


                    nedbetalinger.dato.add(mainJsonObj.get(i).asJsonObject.get("dato").asString)
                    nedbetalinger.gebyrer.add(mainJsonObj.get(i).asJsonObject.get("gebyrer").asFloat)
                    nedbetalinger.renter.add(mainJsonObj.get(i).asJsonObject.get("renter").asFloat)
                    nedbetalinger.restgjeld.add(mainJsonObj.get(i).asJsonObject.get("restgjeld").asFloat)
                    nedbetalinger.total.add(mainJsonObj.get(i).asJsonObject.get("total").asFloat)
                }
                return nedbetalinger
            }
*/




