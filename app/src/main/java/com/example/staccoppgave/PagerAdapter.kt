package com.example.staccoppgave


import android.os.Parcel
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.staccoppgave.fragments.NyttLaanFragment
import com.example.staccoppgave.fragments.WelcomeFragment

class PagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    override fun getCount(): Int {
            return 2
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                WelcomeFragment()
            }
            else -> {
                return NyttLaanFragment()

            }

        }
    }



        override fun getPageTitle(position: Int): CharSequence? {

            return when (position) {
                0 -> "Velkommen"
                else -> {
                    return "Nedbetaling"
                }
            }
        }
}